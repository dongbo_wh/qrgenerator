#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QStandardPaths>
#include <QDebug>
#include <QPainter>
#include <QProgressDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    number_filename = "./pics";
    output_path = "./pics";
    logo_filename = "./pics";

    url = "http://www.xiaoantech.com/";

    ui->textEdit->setReadOnly(true);
    renders = new TcQrencode();
}

MainWindow::~MainWindow()
{
    delete ui;
}

QPixmap text2Pixmap(QString text)
{
    QFont m_font(text, 60, QFont::Black);
    m_font.setFamily("黑体");
    m_font.setPointSize(80);
    m_font.setLetterSpacing(QFont::PercentageSpacing, 130);
    QFontMetrics fmt(m_font);

    QPixmap result(fmt.horizontalAdvance(text), fmt.height());

    QRect rect(0,0,fmt.horizontalAdvance(text), fmt.height());
    result.fill(Qt::transparent);
    QPainter painter(&result);
    painter.setFont(m_font);

    QPen pen = QPen(Qt::black, 10);
    painter.setPen(pen);
    result.fill(Qt::white);
    //painter.drawText(const QRectF(fmt.width(text), fmt.height()),Qt::AlignLeft, text);

    painter.drawText((const QRectF)(rect), text);

    //两边留白
    QPixmap res(result.width() + 20, result.height());
    res.fill(Qt::white);
    QPainter resPainter(&res);
    resPainter.drawPixmap((res.width() - result.width()) / 2, 0, result);

    return res.scaled(800, 140, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
}

QPixmap splice_pixmap(QPixmap qrcode, QPixmap number)
{

    QPixmap temp(800, qrcode.height() + number.height());
    temp.fill(Qt::white);
    QPainter tempPainter(&temp);

    tempPainter.drawPixmap(0, 0, qrcode);
    tempPainter.drawPixmap(0, qrcode.height(), number);

     //留白
    QPixmap ret(temp.width() + 50, temp.height() + 50);
    ret.fill(Qt::white);
    QPainter retPainter(&ret);
    retPainter.drawPixmap(25, 25, temp);

    return ret;
}

//添加logo
QPixmap MainWindow::qrcode_add_logo(QPixmap qrcode)
{
    QImage logo(ui->logo_file_lineEdit->text());
    QPixmap pix_logo = QPixmap::fromImage(logo);
    int logo_w = qrcode.width() / 20 * 7;
    int logo_h = qrcode.height() / 20 * 7;
    pix_logo = pix_logo.scaled(logo_w, logo_h, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    //计算起点
    int logo_x = (qrcode.width() / 2) - (pix_logo.width() / 2);
    int logo_y = (qrcode.width() / 2) - (pix_logo.width() / 2);

    QPainter qrPainter(&qrcode);
    qrPainter.drawPixmap(logo_x, logo_y, pix_logo);
    return qrcode;
}

//输出按钮
void MainWindow::on_pushButton_clicked()
{
    QFile file(ui->number_file_lineEdit->text());
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << file.errorString();
        return;
    }

    QTextStream * out = new QTextStream(&file);//文本流
    QStringList tempOption = out->readAll().split("\n", QString::SkipEmptyParts);//每行以\n区分

    //url处理


    QProgressDialog *progress = new QProgressDialog(NULL, "取消", 0, tempOption.count(), this);
    progress->setWindowModality(Qt::WindowModal);

    progress->setWindowTitle(tr("生成进度"));
    progress->setMinimumSize(400, 50);
    progress->setMinimumDuration(1);
    progress->show();

    for(int i = 0; i < tempOption.count(); i++)
    {
        QStringList tempbar = tempOption.at(i).split("\r", QString::SkipEmptyParts);//一行中的单元格以，区分
        QString number = tempbar.at(0);

        url = ui->url_lineEdit->text().replace("***", number);
         qDebug() << "url:" << url;

        //生成二维码
        QImage a = renders->encodeImage(url, 40);
        QImage  qrcode = a.scaled(800, 800, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

        //qDebug() << tr("qrcode[%1 * %2]").arg(qrcode.width()).arg(qrcode.height());

         QPixmap qrcode_logo(QPixmap::fromImage(qrcode));

        //添加logo
        if(!(ui->logo_file_lineEdit->text().isNull()))
        {
            qrcode_logo = qrcode_add_logo(QPixmap::fromImage(qrcode));
        }

        //字符串图片
        QPixmap numPixmap = text2Pixmap(number);
        //qDebug() << tr("numPixmap[%1 * %2]").arg(numPixmap.width()).arg(numPixmap.height());

        //拼接
        QPixmap completed = splice_pixmap(qrcode_logo, numPixmap);

        //ui->label_2->setPixmap((completed));
        completed.save(output_path + "/" + number + ".jpg", "JPG", 100);

        progress->setValue(i);
        if(progress->wasCanceled())
        {
            break;
        }
    }

    progress->deleteLater();
    progress = NULL;

}

//导入编号列表文件
void MainWindow::on_pushButton_2_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("选择文件"),
                                                    number_filename,
                                                    tr("csv文件(*.csv);;"
                                                       "本本文件(*.txt);;"
                                                       "全部(*.*)"));
    if(!filename.isNull())
    {
        ui->number_file_lineEdit->setText(filename);
        number_filename = filename;
    }
}

//选择输出目录
void MainWindow::on_pushButton_3_clicked()
{
    QString dirpath = QFileDialog::getExistingDirectory(this,
                                                    "选择目录",
                                                    output_path,
                                                    QFileDialog::ShowDirsOnly);
    if(!dirpath.isNull())
    {
        ui->output_lineEdit->setText(dirpath);
        output_path = dirpath;
    }
}

//选择logo
void MainWindow::on_pushButton_4_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("选择文件"),
                                                    logo_filename,
                                                    tr("全部(*.*)"));
    if(!filename.isNull())
    {
        ui->logo_file_lineEdit->setText(filename);
        logo_filename = filename;
    }
}

